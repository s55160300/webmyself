﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Net.Mail;
using WebApplication1.InternalService;

namespace WebApplication1.Service
{
    public class EmailService
    {
        private readonly IConfiguration configuration;

        public EmailService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void SendEmail()
        {
            string host = configuration.GetSection("SMTPServer").GetSection("Host").Value;
            string port = configuration.GetSection("SMTPServer").GetSection("Port").Value;
            string username = configuration.GetSection("SMTPServer").GetSection("Username").Value.Base64DecodeString();
            string password = configuration.GetSection("SMTPServer").GetSection("Password").Value.Base64DecodeString();
            SmtpClient smtpClient = new SmtpClient
            {
                Host = host,
                Port = Convert.ToInt32(port),
                EnableSsl = true,
                Credentials = new NetworkCredential(username, password)
        };
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(username);
            mailMessage.To.Add("s55160300@gmail.com");
            mailMessage.Body = "body";
            mailMessage.Subject = "subject";
            smtpClient.Send(mailMessage);
        }
    }
}
