﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace WebApplication1.InternalService
{
    public static class Utilities
    {
        public static string Base64DecodeString(this string value)
        {
            byte[] getByte = Convert.FromBase64String(value);
            string deCodeString = System.Text.Encoding.UTF8.GetString(getByte);

            return deCodeString;
        }
        public static string Base64EncodeString(this string value)
        {
            byte[] getByte = Encoding.Unicode.GetBytes(value);
            string encodeString = Convert.ToBase64String(getByte);

            return encodeString;
        }
    }
}
