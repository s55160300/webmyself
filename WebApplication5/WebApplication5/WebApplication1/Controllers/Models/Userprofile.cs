﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Controllers.Models
{
    public class Userprofile
    {
        private int id = 0;
        private string firstName = string.Empty;
        private string lastName = string.Empty;
        private string username = string.Empty;
        private string password = string.Empty;
        private DateTime? createDate = new DateTime();
        private string error = string.Empty;

        public int Id { get => id; set => id = value; }
        public string FirstName { get => firstName ?? string.Empty; set => firstName = value; }
        public string LastName { get => lastName ?? string.Empty; set => lastName = value; }
        public string Username { get => username ?? string.Empty; set => username = value; }
        public string Password { get => password ?? string.Empty; set => password = value; }
        public DateTime? CreateDate { get => createDate; set => createDate = value; }
        public string Error { get => error; set => error = value; }

        public string Fullname()
        {
            return firstName + " " + lastName;
        }
    }
}
