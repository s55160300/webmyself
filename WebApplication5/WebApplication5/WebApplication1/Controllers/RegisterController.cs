﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApplication1.Controllers.Models;
using WebApplication1.Repository.Models;

namespace WebApplication1.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IConfiguration configuration;

        public RegisterController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveData(Userprofile userprofile)
        {
            UserProfileRepository repo = new UserProfileRepository(configuration);
            userprofile = repo.Create(userprofile);
            
            return Json(new { url = Url.Action("Index", "SignIn"), error = userprofile.Error});

        }
    }
}