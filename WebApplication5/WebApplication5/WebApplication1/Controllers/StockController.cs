﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    public class StockController : Controller
    {
        public IActionResult Index()
        {
            ViewData["PageName"] = "Main";
            return View();
        }
        public IActionResult CreateUser()
        {
            ViewData["PageName"] = "Create User";
            return View();
        }
    }
}