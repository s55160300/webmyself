﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using WebApplication1.Controllers.Models;
using WebApplication1.Repository.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    public class SignInController : Controller
    {
        private readonly IConfiguration configuration;

        public SignInController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CheckLogin(Userprofile userprofile)
        {
            UserProfileRepository repo = new UserProfileRepository(configuration);
            userprofile = repo.GetLogin(userprofile);

            if (userprofile.Id != 0)
            {
                HttpContext.Session.SetString("Fullname", userprofile.Fullname());
            }

            return Json(new { url = Url.Action("Index", "Stock"), error = userprofile.Error });

        }

        [HttpGet]
        public void SendEmail()
        {
            EmailService emailService = new EmailService(configuration);
            emailService.SendEmail();
        }
    }
}