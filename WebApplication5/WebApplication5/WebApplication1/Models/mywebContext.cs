﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using WebApplication1.InternalService;

namespace WebApplication1.Models
{
    public partial class mywebContext : DbContext
    {

        private readonly IConfiguration configuration;

        public mywebContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public mywebContext(DbContextOptions<mywebContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Userprofile> Userprofile { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = configuration.GetSection("Connectionstring").Value.Base64DecodeString();
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "adminmyweb");

            modelBuilder.Entity<Userprofile>(entity =>
            {
                entity.ToTable("Userprofile", "dbo");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
