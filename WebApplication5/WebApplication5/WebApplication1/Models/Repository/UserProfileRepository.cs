﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Repository.Models
{
    public class UserProfileRepository
    {
        private readonly IConfiguration configuration;

        public UserProfileRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public Controllers.Models.Userprofile Create(Controllers.Models.Userprofile userprofile)
        {

            try
            {
                using (mywebContext context = new mywebContext(configuration))
                {
                    Userprofile dataDb = new Userprofile()
                    {
                        FirstName = userprofile.FirstName,
                        LastName = userprofile.LastName,
                        Username = userprofile.Username,
                        Password = userprofile.Password,
                        CreateDate = DateTime.Now
                    };

                    context.Userprofile.Add(dataDb);
                    context.SaveChanges();
                    userprofile.Id = dataDb.Id;
                    userprofile.FirstName = dataDb.FirstName;
                    userprofile.LastName = dataDb.LastName;
                }
            }
            catch (Exception ex)
            {
                userprofile.Error = ex.Message;
            }

            return userprofile;
        }

        public Controllers.Models.Userprofile GetLogin(Controllers.Models.Userprofile userprofile)
        {
            try
            {
                using (mywebContext context = new mywebContext(configuration))
                {
                    Userprofile dataDb = context.Userprofile.Where(up => up.Username == userprofile.Username && up.Password == userprofile.Password).FirstOrDefault();

                    if (dataDb != null)
                    {
                        userprofile.Id = dataDb.Id;
                        userprofile.FirstName = dataDb.FirstName;
                        userprofile.LastName = dataDb.LastName;
                    }
                }
            }
            catch (Exception ex)
            {
                userprofile.Error = ex.Message;
            }

            return userprofile;
        }
    }
}
